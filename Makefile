CC = gcc
CFLAGS = -std=c18 -pedantic -Wall -Werror

ASM = nasm
ASMFLAGS = -g -felf64

LDFLAGS = -lm -no-pie

BUILD_DIR = ./build
SRC_DIR = ./src
FORMAT_DIR:= $(SRC_DIR)/formats
ACTION_DIR:= $(SRC_DIR)/actions

TARGET = image_editor

SOURCES = $(shell find $(SRC_DIR) -name *.c)
OBJECTS = $(SOURCES:%.c=$(BUILD_DIR)/%.o)

ASM_SRC = $(SRC_DIR)/actions/sepia.asm
ASM_OBJ = $(BUILD_DIR)/$(ACTION_DIR)/sepia.asm.o

.PHONY: all clean

all: dir $(BUILD_DIR)/$(TARGET)

dir:
	@mkdir -p $(BUILD_DIR)/$(SRC_DIR) $(BUILD_DIR)/$(FORMAT_DIR) $(BUILD_DIR)/$(ACTION_DIR)

$(ASM_OBJ): $(ASM_SRC)
	$(ASM)  $< $(ASMFLAGS) -o $@

$(OBJECTS): $(BUILD_DIR)/%.o : %.c
	$(CC) -c $< -o $@ $(CFLAGS)

$(BUILD_DIR)/$(TARGET): $(OBJECTS) $(ASM_OBJ)
	$(CC) $^ -o $@ $(LDFLAGS)

clean:
	rm -r $(BUILD_DIR)
