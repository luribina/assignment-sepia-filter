# assignment-8-sepia

## Небольшое пояснение

По сути же это прошлая лаба с поворотом картинки, поэтому здесь лежат файлы и с другими эффектами


## Usage

Использовать C имплементацию
```
    ./image_editor -a sepia: -f bmp INPUT_FILE OUTPUT_FILE
```

Использовать ASM имплементацию
```
    ./image_editor -a sepia_asm: -f bmp INPUT_FILE OUTPUT_FILE
```

Запустить тест, для этого в директории build должен лежать файл test.bmp <s>да, имя файла просто захардкожено</s>
```
    ./image_editor -t
```

Вывод теста для бмп на 96МБ
```
Average time elapsed in microseconds for C  : 624540
Average time elapsed in microseconds for ASM: 252342
```
