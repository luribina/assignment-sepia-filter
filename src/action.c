#include "action.h"
#include "actions/negative.h"
#include "actions/rotate.h"
#include "actions/sepia.h"

typedef struct image action(struct image * image, const struct string * arg);

static action * const action_handler[] = {
        [ROTATE] = rotate,
        [NEGATIVE] = negative,
        [SEPIA] = sepia,
        [SEPIA_ASM] = sepia_asm,
};

struct image do_action(struct image * image, enum action action, const struct string * arg) {
    return action_handler[action](image, arg);
}
