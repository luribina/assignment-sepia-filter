global sepia_four_pixels

section .rodata

align 16
blue_coef: dd 0.131, 0.168, 0.189, 0.131
align 16
green_coef: dd 0.543, 0.686, 0.769, 0.543
align 16
red_coef: dd  0.272, 0.349, 0.393, 0.272

align 16
max: dd 255.0, 255.0, 255.0, 255.0

section .text

%define blue_arg rdi
%define green_arg rsi
%define red_arg rdx

%define blue_reg xmm0
%define green_reg xmm1
%define red_reg xmm2
%define coef_reg xmm3

%define sepia rcx

%define color_shuf1 0b01000000
%define color_shuf2 0b10100101
%define color_shuf3 0b11111110

%define coef_shuf1 0b11100100
%define coef_shuf2 0b01001001
%define coef_shuf3 0b10010010

; %1 - shufps argument for color array
; %2 - shufps argument for coef array
; %3 - color
%macro do_color 3
    movdqa %3_reg, [%3_arg]
    movaps coef_reg, [%3_coef]
    shufps coef_reg, coef_reg, %2
    shufps %3_reg, %3_reg, %1
    mulps %3_reg, coef_reg
%endmacro

; %1 - shufps argument for color array
; %2 - shufps argument for coef array
%macro do_iter 2
    do_color %1, %2, blue
    do_color %1, %2, green
    do_color %1, %2, red
    call set_result
%endmacro

set_result:
    addps blue_reg, green_reg
    addps blue_reg, red_reg
    minps blue_reg, [max]
    cvtps2dq blue_reg, blue_reg

    pextrb [sepia], blue_reg, 0
    pextrb [sepia + 1], blue_reg, 4
    pextrb [sepia + 2], blue_reg, 8
    pextrb [sepia + 3], blue_reg, 12
    ret

; rdi b1 b2 b3 b4
; rsi g1 g2 g3 g4
; rdx r1 r2 r3 r4
; rcx result
sepia_four_pixels:
    do_iter color_shuf1, coef_shuf1
    add sepia, 4

    do_iter color_shuf2, coef_shuf2
    add sepia, 4

    do_iter color_shuf3, coef_shuf3
    ret
