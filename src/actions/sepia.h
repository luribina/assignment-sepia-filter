#pragma once

#include "../image.h"
#include "../string_struct.h"

struct image sepia(struct image * image, const struct string * arg);

struct image sepia_asm(struct image * image, const struct string * arg);
